module.exports = function (grunt) {
  grunt.loadNpmTasks("grunt-babel");
  grunt.initConfig({
    babel: {
      options: {
        sourceMap: true,
        presets: ["@babel/preset-env"],
      },
      dist: {
        files: {
          "dist/app.js": "src/app.js",
        },
      },
    },
  });

  grunt.registerTask("default", ["babel"]);
};
